<?php
require_once "animal.php";
require_once "Ape.php";
require_once "Frog.php";


$sheep = new Animal("shaun");

echo "Name = ".$sheep->name."<br>";
echo "Legs = ".$sheep->legs."<br>";
echo "Cold blooded = ".$sheep->cold_blooded."<br><br><br>";



$kodok = new Frog("buduk");
echo "Name = ".$kodok->name."<br>";
echo "Legs = ".$kodok->legs."<br>";
echo "Cold blooded = ".$kodok->cold_blooded."<br>";
echo "Jump = ".$kodok->jump()."<br><br><br>" ; // "hop hop"


$sungokong = new Ape("kera sakti");
echo "Name = ".$sungokong->name."<br>";
echo "Legs = ".$sungokong->legs."<br>";
echo "Cold blooded = ".$sungokong->cold_blooded."<br>";
echo "Yell = ".$sungokong->yell();// "Auooo"




// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
